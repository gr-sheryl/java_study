package com.company.pojo;

import lombok.Data;

/**
 * author: Sheryl
 * create time: 2022/4/6 9:50 下午
 */
@Data
public class Student {
    private int id;
    private String name;
    private int tid;
}
