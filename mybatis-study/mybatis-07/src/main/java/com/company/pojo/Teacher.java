package com.company.pojo;

import lombok.Data;

import java.util.List;

/**
 * author: Sheryl
 * create time: 2022/4/6 9:52 下午
 */

@Data
public class Teacher {
    private int id;
    private String name;
    private List<Student> students;
}
