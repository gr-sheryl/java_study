package com.company.dao;

import com.company.pojo.Teacher;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * author: Sheryl
 * create time: 2022/4/6 9:54 下午
 */
public interface TeacherMapper {
    List<Teacher> getTeacher(int id);

    List<Teacher> getTeacher2(int id);

}
