package com.company.util;

import org.junit.Test;

import java.util.UUID;
/**
 * author: Sheryl
 * create time: 2022/4/8 3:43 下午
 */
public class IDUtils {
    public static String getId(){
        return UUID.randomUUID().toString().replaceAll("-","");
    }

    @Test
    public void test(){
        System.out.println(IDUtils.getId());
        System.out.println(IDUtils.getId().length());
    }

}
