package com.company.dao;

import com.company.pojo.Blog;

import java.util.List;
import java.util.Map;

/**
 * author: Sheryl
 * create time: 2022/4/8 3:40 下午
 */
public interface BlogMapper {
    int addBlog(Blog blog);

    //if查询
    List<Blog> queryBlogIF(Map map);

    //set更新
    int updateBlog(Map blog);

    //查询1，2，3号的记录
    List<Blog> queryBlogForeach(Map map);
}
