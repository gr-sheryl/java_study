package com.company.pojo;

import lombok.Data;

import java.util.Date;

/**
 * author: Sheryl
 * create time: 2022/4/8 3:37 下午
 */
@Data
public class Blog {
    private String id;
    private String title;
    private String author;
    private Date createTime;
    private int views;
}
