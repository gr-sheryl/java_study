package com.company.dao;

import com.company.pojo.User;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * author: Sheryl
 * create time: 2022/4/3 4:58 下午
 */
public interface userMapper {
    //查询全部用户
    @Select("select * from user")
    List<User> getUserList();

    //根据ID查询用户
    //方法存在多个参数，所有的参数前面必须加上@Param("id")注解
    @Select("select * from user where id = #{id}")
    User getUserByID(@Param("id") int id);

    //insert
    @Insert("insert into user values (#{id}, #{name}, #{password})")
    int addUser(User user);

    //update
    @Update("update user set name=#{name}, pwd=#{password} where id=#{id}")
    int updateUser(User user);

    //delete
    @Delete("delete from user where id=#{uid}")
    int deleteUser(@Param("uid") int id);

    //分页查询
    List<User> getUserByLimit(Map<String, Object> map);

}
