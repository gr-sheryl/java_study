package com.company.dao;


import com.company.pojo.User;
import com.company.util.mybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * author: Sheryl
 * create time: 2022/4/3 5:20 下午
 */
public class userMapperTest {
    @Test
    public void getUserByIDTest(){
        SqlSession session = mybatisUtils.getSqlSession();
        SqlSession session2 = mybatisUtils.getSqlSession();
        userMapper mapper = session.getMapper(userMapper.class);
        userMapper mapper2 = session2.getMapper(userMapper.class);
        User user = mapper.getUserByID(1);
        System.out.println(user);
        session.close();
        User user2 = mapper2.getUserByID(1);
        System.out.println(user2);
        System.out.println(user==user2);
        session2.close();

    }

}
