package com.company.dao;

import com.company.pojo.User;

import java.util.List;
import java.util.Map;

/**
 * author: Sheryl
 * create time: 2022/4/3 4:58 下午
 */
public interface userMapper {
    //根据ID查询用户
    User getUserByID(int id);

    int updateUser(User user);

}
