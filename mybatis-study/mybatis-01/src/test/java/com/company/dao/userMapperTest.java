package com.company.dao;

import com.company.pojo.User;
import com.company.util.mybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * author: Sheryl
 * create time: 2022/4/3 5:20 下午
 */
public class userMapperTest {
    @Test
    public void test(){
        //获得SqlSession对象
        try (SqlSession sqlSession = mybatisUtils.getSqlSession()) {
            //执行sql
            userMapper mapper = sqlSession.getMapper(userMapper.class);
            List<User> userList = mapper.getUserList();

            for (User user : userList) {
                System.out.println(user);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void getUserByIDTest(){
        try(SqlSession sqlSession = mybatisUtils.getSqlSession()) {
            userMapper mapper = sqlSession.getMapper(userMapper.class);
            User user = mapper.getUserByID(1);
            System.out.println(user);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    //增删改需要提交事务
    public void addUserTest(){
        try(SqlSession sqlSession = mybatisUtils.getSqlSession()) {
            userMapper mapper = sqlSession.getMapper(userMapper.class);
            int res = mapper.addUser(new User(4, "李六", "89437957"));
            sqlSession.commit();
            if(res > 0){
                System.out.println(res + "  insert sucess!");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void updateUserTest(){
        try(SqlSession sqlSession = mybatisUtils.getSqlSession()){
            userMapper mapper = sqlSession.getMapper(userMapper.class);
            int res = mapper.updateUser(new User(1, "你好","ahjkffj3"));
            sqlSession.commit();
            if(res > 0){
                System.out.println(res + "  update sucess!");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void deleteUserTest(){
        try(SqlSession sqlSession = mybatisUtils.getSqlSession()){
            userMapper mapper = sqlSession.getMapper(userMapper.class);
            int res = mapper.deleteUser(3);
            sqlSession.commit();
            if(res > 0){
                System.out.println(res + "  delete sucess!");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    //增删改需要提交事务
    public void addUser2Test(){
        try(SqlSession sqlSession = mybatisUtils.getSqlSession()) {
            userMapper mapper = sqlSession.getMapper(userMapper.class);
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("uid", 3);
            map.put("uname", "王武");
            map.put("upwd", "akfjeh7");
            int res = mapper.addUser2(map);
            sqlSession.commit();
            if(res > 0){
                System.out.println(res + "  insert2 sucess!");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void getUserByID2Test(){
        try(SqlSession sqlSession = mybatisUtils.getSqlSession()) {
            userMapper mapper = sqlSession.getMapper(userMapper.class);
            Map<String, Object> map = new HashMap<>();
            map.put("uid", 1);
            User user = mapper.getUserByID2(map);
            System.out.println(user);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void getUserLikeTest(){
        try(SqlSession sqlSession = mybatisUtils.getSqlSession()) {
            userMapper mapper = sqlSession.getMapper(userMapper.class);
            List<User> userList = mapper.getUserLike("李%");
            for(User user: userList){
                System.out.println(user);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
