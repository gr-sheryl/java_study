package com.company.dao;

import com.company.pojo.Student;
import com.company.pojo.Teacher;
import com.company.util.mybatisUtils;
import org.apache.ibatis.session.SqlSession;
import  org.junit.Test;

import java.util.List;

/**
 * author: Sheryl
 * create time: 2022/4/6 10:04 下午
 */

public class MyTest {
    @Test
    public void getTeacherTest(){
        try(SqlSession sqlSession = mybatisUtils.getSqlSession()) {
            TeacherMapper mapper = sqlSession.getMapper(TeacherMapper.class);
            Teacher teacher = mapper.getTeacher(1);
            System.out.println(teacher);
        }
    }

    @Test
    public void getStudentTest(){
        try(SqlSession sqlSession = mybatisUtils.getSqlSession()) {
            StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);
            List<Student> student = mapper.getStudent2();
            for (Student s:student){
                System.out.println(s);
            }
        }
    }
}
