package com.company.dao;

import com.company.pojo.Teacher;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * author: Sheryl
 * create time: 2022/4/6 9:54 下午
 */
public interface TeacherMapper {
    @Select("select * from teacher where id = #{id}")
    Teacher getTeacher(@Param("id")int id);
}
