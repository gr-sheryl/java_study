package com.company.pojo;

import lombok.Data;

/**
 * author: Sheryl
 * create time: 2022/4/6 9:50 下午
 */
@Data
public class Student {
    private int id;
    private String name;
    //学生关联老师（使用teacher, 而不直接用int）
    private Teacher teacher;
}
