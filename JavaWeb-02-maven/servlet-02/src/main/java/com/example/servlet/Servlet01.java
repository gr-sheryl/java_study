package com.example.servlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * author: Sheryl
 * create time: 2022/3/21 5:39 下午
 */
public class Servlet01 extends HttpServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        InputStream is = this.getServletContext().getResourceAsStream("/WEB-INF/classes/com/example/servlet/ja.properties");
        Properties prop = new Properties();
        prop.load(is);
        String user = prop.getProperty("user");
        String pwd = prop.getProperty("password");
        resp.getWriter().print(user + ":" + pwd);
    }

}

