package com.compamy.servlet;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * author: Sheryl
 * create time: 2022/3/23 2:59 下午
 */
public class FileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //获取下载文件路径
        //String realPath = this.getServletContext().getRealPath("/qq.jpg");
        String realPath = "/Users/apple/IdeaProjects/JavaWeb-01-maven/JavaWeb-02-maven/response/src/main/resources/qq.jpg";
        System.out.println("path:" + realPath);
        String fileName = realPath.substring(realPath.lastIndexOf("//")+1);
        resp.setHeader("Content-Disposition","attachment; filename=" + fileName);
        FileInputStream in = new FileInputStream(realPath);
        int len = 0;
        byte[] buffer = new byte[1024];
        ServletOutputStream out = resp.getOutputStream();
        while((len = in.read(buffer)) > 0){
            out.write(buffer, 0, len);
        }
        in.close();
        out.close();

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
