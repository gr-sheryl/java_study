package com.company.servlet;

import javax.servlet.*;
import java.io.IOException;

/**
 * author: Sheryl
 * create time: 2022/3/25 5:32 下午
 */
public class ServletFilter implements Filter {
    @Override
    //初始化：在Web服务器启动时就初始化，因为要随时监听，处理请求
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("Filter已经初始化");
    }

    /*
    1.过滤所有的代码，过滤特定的请求时都会执行
    2.必须让过滤器继续执行（加上chain.doFilter(req, resp);）
     */
    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        //解决中文乱码
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("text/html");

        //请求继续执行，否则请求被拦截停止
        chain.doFilter(req, resp);

    }

    @Override
    //销毁:web服务器关闭的时候销毁
    public void destroy() {
        System.out.println("Filter已经销毁");
    }
}
