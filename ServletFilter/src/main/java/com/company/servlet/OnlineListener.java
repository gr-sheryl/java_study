package com.company.servlet;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * author: Sheryl
 * create time: 2022/3/25 10:33 下午
 */
//统计网站在线人数，即统计session（每个用户对应一个session）
public class OnlineListener implements HttpSessionListener {
    @Override
    //创建sessionListener
    //一旦创建就会触发监听
    public void sessionCreated(HttpSessionEvent se) {
        System.out.println(se.getSession().getId());
        ServletContext ctx = se.getSession().getServletContext();
        Integer onlineCount = (Integer) ctx.getAttribute("OnlineCount");
        if (onlineCount == null){
            onlineCount = new Integer(1);
        }else{
            int cnt = onlineCount.intValue();
            onlineCount = new Integer(cnt+1);
        }

        ctx.setAttribute("OnlineCount", onlineCount);
    }

    @Override
    //销毁sessionListener
    public void sessionDestroyed(HttpSessionEvent hse) {
        System.out.println("OnlineCount销毁进入销毁");
        ServletContext ctx = hse.getSession().getServletContext();
        Integer onlineCount = (Integer) ctx.getAttribute("OnlineCount");
        if (onlineCount == null){
            onlineCount = new Integer(1);
        }else{
            int cnt = onlineCount.intValue();
            onlineCount = new Integer(cnt-1);
        }

        ctx.setAttribute("OnlineCount", onlineCount);
        hse.getSession().invalidate();
        System.out.println("OnlineCount销毁");
    }
}
