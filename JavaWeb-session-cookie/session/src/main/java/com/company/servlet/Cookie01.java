package com.company.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Date;

/**
 * author: Sheryl
 * create time: 2022/3/24 11:09 下午
 */
public class Cookie01 extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");

        PrintWriter out = resp.getWriter();
        Cookie[] cookies = req.getCookies();
        if (cookies != null){
            out.write("last visit time");
            for (int i = 0; i < cookies.length; i++) {
                Cookie cookie = cookies[i];
                if(cookie.getName().equals("last_time")){
                    long lasttime = Long.parseLong(cookie.getValue());
                    Date date = new Date(lasttime);
                    out.write(date.toString());

                }
            }
        }else{
            out.write("first visit!");
        }
        Cookie cookie = new Cookie("last_time", URLDecoder.decode("你好","utf-8"));
        cookie.setMaxAge(60 * 60 * 24);

        resp.addCookie(cookie);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
